import math

# F-Strings
name = "Davis"
age = 32
greeting = f"Hello {name}, you are {age} years old."

print(greeting)

x = 43
y = 65
result = f"The sum of {x} and {y} is {x + y}"
print(result)

pi_value = f"Pi value up to 10 decimal places: {math.pi:.10f}"
print(pi_value)

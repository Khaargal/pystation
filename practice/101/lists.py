# generate a list of integers divisible by 5 from 0 - 100
numbers = range(0, 100)

five_modulos = [n for n in numbers if n % 5 == 0]
print(five_modulos)

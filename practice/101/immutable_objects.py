# “For immutable objects like strings or integers, Python doesn’t modify the variable value beyond the function’s scope”

x = 5


def update_number(n: int) -> None:
    x = 10
    print("Inside the method x : ", x)
    print("Inside the method n : ", n)


update_number(x)
print("Outside the method : ", x)

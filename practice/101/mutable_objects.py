list_of_numbers = [5]

print(list_of_numbers)


def update_list(numbers: list[int], n: int) -> None:
    numbers.append(n)


update_list(list_of_numbers, 89)
print(list_of_numbers)

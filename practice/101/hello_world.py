import unittest


def hello():
    return "Hello, World!"


class HelloWorldTest(unittest.TestCase):
    def test_hello(self):
        self.assertEqual(hello(), "Hello, World!")

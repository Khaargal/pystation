import json

# Fix: ModuleNotFoundError: No module named 'libs'
from libs.person import Person


def person_serializer(obj: any) -> dict:
    if isinstance(obj, Person):
        return {"name": obj.name, "age": obj.age}
    raise TypeError(f"Type {type(obj)} not seriazable")


john = Person("John", 32, "john@users.com")

print(john.name)
print(json.dumps(john, default=person_serializer))

person = {"name": "John", "age": 30}
print("person's type is: ", type(person))

# serialization of the object”
person_json = json.dumps(person)
print("person_json's type is: ", type(person_json))
print(person_json)

# deserialization of the json, loads returns a dictionary
person_dict = json.loads(person_json)
print(person_dict)
assert person == person_dict

"""
Learning Python
"""

from datetime import datetime

current_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def greetings(first_name, last_name):
    full_name = f"{first_name} {last_name}"
    message = f"Hello, {full_name.title()}!\nWe are the {current_datetime}"

    print(message)


greetings("john", "smith")

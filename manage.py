from pathlib import Path

# BASE_DIR is the project's root
BASE_DIR = Path(__file__).resolve().parent

DATA_ROOT = BASE_DIR / "data"

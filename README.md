# pystation

## Setting up the app

Clone the repo:

```sh
❯ git clone https://gitlab.com/Khaargal/pystation.git
```

## Development

Make sure you have python3.12.2 installed. If not, run:

```sh
❯ brew install asdf
❯ asdf plugin-add python
❯ asdf install python 3.12.2
❯ asdf global python 3.12.2
```

### Install virtualenv manager

```sh
❯ pip install --user pipenv
```

### Install project dependencies

```sh
❯ pipenv install
```

### Activate that environment

```sh
❯ pipenv shell
```

### Install new package

```sh
❯ pipenv install name-of-package
```

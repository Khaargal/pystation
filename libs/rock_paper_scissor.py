import random

computer_choice = random.choice(["scissors", "rock", "paper"])
user_choice = random.choice(["scissors", "rock", "paper"]) #input("Do you want rock, paper, or scissors?")

if computer_choice == user_choice:
    print("Tie!")
elif computer_choice == "scissors" and user_choice == "rock":
    print("You WIN!")
elif computer_choice == "paper" and user_choice == "scissors":
    print("You WIN!")
elif computer_choice == "rock" and user_choice == "paper":
    print("You WIN!")
else:
    print("You LOSE!")
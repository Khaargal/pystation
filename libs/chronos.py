from datetime import datetime, timedelta

current_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
print(current_datetime)

a_week_from_now = datetime.now() + timedelta(days=7)
print(a_week_from_now)

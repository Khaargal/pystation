# Returns your age in decades and years

def age_in_decades():
    age = int(input("How old are you?\n"))
    decades = age // 10
    years = age % 10
    print("You are " + str(decades) + " and " + str(years) + " years old.")


age_in_decades()
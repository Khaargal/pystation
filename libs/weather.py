temperature = 29
forecast = "rainy"

if temperature < 30 and forecast != "rainy":
    print("Not too hot, enjoy the outdoor.")
elif temperature < 5:
    print("Bit cold, wear something warm.")
elif temperature < 30 and forecast == "rainy":
    print("Bring an umbrealla.")
else:
    print("It's too hot!")
    print("Stay inside!")
import random

def roll_dice():
    return random.randint(1, 6)


def guess_my_roll():
    roll = roll_dice()
    guess = int(input("Guess the dice roll:\n"))

    if roll == guess:
        print("You guested it.")
    else:
        print("NOPE!")

    print("The roll is: " + str(roll))


guess_my_roll()
